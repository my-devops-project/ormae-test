# DevOps Engineer Test

## Chalk out a Git repository structure in terms of helping in reaching maximum efficiency in terms of ease of development and collaborating. 

For each microservice, we can have separate folder under same repository in Git. 

```

My application (Git Repository)
    - microservice-1 (one folder)
    - microservice-2 (other folder)
    and so on

```

By this way, we have all code base of my application under same repository. It paves way to share common components across multiple services.

Also, it imparts collaboration among teams to understand all services and helps to test entire application. 

We could have separate repo for each microservice. With that each team will own the repo and it will make easy when it comes to merges. But it has few drawbacks like lack of collaboration, chances of more bugs etc.,


### Branching strategy:-

Each Microservice team will have their own project branch created from development. Developers from respective teams will create feature branch out of it & make their changes and raise MR.

Once all developers done with their changes, DevLeads will accept the MR. Then MR will be raised against development.

Once the developed code is well tested and ready, It will get merged to Master.

```
        master (prod) 
        development
            - project branches
                - feature branches

```

## Lay down a plan for CI/CD. As there will be tests pertaining to each microservice, how do you plan to run them in tandem in the deployment pipeline. 

Since we have all our code under same repo, it makes build & deployment quite easier. To decouple the build configs, let's have independent repo to setup configuration details.

    > JSON ---> To have repo and project details
    > Jenkinsfile(s)

When it comes to Continuous Integration, we can have two kinds of build/deployment.

    1. Day build (Whenever there is merge request)
        
        This build ensures the code is clean and it can be merged back to parent branch (project/development/master)

        - MR Feature to Project
        - MR Project to Development
        - MR Development to Master

    2. Nightly build (Once MR gets accepted) T
        
        There can be more than one merge happened to the repo in a day. We will keep track of each merge happens and auto schedule one build at night for each branch. 

These builds will get Auto-triggered by Git Webhooks (Merge request events). And developers & leads will get notification via email for every successful/failure builds.


### Build Stages:-

```
    Master Pipeline (configured to build config repo)
    Deduce the tag and pass it to all build products.
    - Build Products
        - product-1 (Child Job)
            - Git Checkout
            - Build the code
            - Code coverage
            - Unit Test
            - Post successful, bake them into Docker images
            - Push the images to Docker hub or Jfrog Artifactory
        - product-2 (Child Job)
            - Git Checkout
            - Build the code
            - Code coverage
            - Unit Test
            - Post successful, bake them into Docker images
            - Push the images to Docker hub or Jfrog Artifactory
    - Sends notification to concerned Development team about build/stage failures or Success


```

## Prepare a deployment strategy that can be adapted for Staging, UAT and Production. Note that staging setup needs to be less in terms of operating cost. 

**Terraform + Kubernetes**

We can make use of Terraform (IaaC) to setup Kubernetes Cluster and Orchestrate Containers with ease.

Since Terraform is Cloud Agnostic tool, It is more powerful interms of collaborating with various providers as per our requirement.

We write TF config file to setup Kubernetes Cluster and deploy our apps.

For Lower environments like staging, we can spin up few containers by taking operartional cost in mind. When our apps are ready to deploy in PROD environment, we can take advantage of cloud providers like AWS/Azure/GCP.

We can use same TF-config file with few modifications to deploy them into higher environments.

With AWS, we can automaticallly scale out the deployment based on demand. This can be forecasted with help of CloudWatch Alarms. And we need to pay per use.


## Prepare a plan to see logs in all the above three environments. 

In Terms of monitoring, We can either utilize the AWS CloudWatch utility or use other vendors like Splunk, Prometheus + Grafana (for virtualization)

We can configure Prometheus to get logs from Kubernetes components. Prometheus is basically a pull based mechanism in which once we have the targets defined, it will check the health of each target and sends back the response.

Because of pull based mechanism, it will not overloaded with bunch of logs.

Then, These logs can be computed in the form of Digitalized Chart using Grafana.


## Prepare a plan to add alerts and monitoring across all of the mentioned components.

We can set alerts on below things, by which we can forecast and avoid potential downtime in PROD environments.

> Node level
    - host is down
    - Disk Usage
    - CPU/memory usage

> Application level
    - If health check fails, i.e. Application throws response code other than 200.
    - No of hits / Traffic  

---

User Interface - We can build & deploy UI on web server using Terraform. Terraform has access to 100+ providers including Git, AWS. We can get our code cloned, built & deployed.

API Gateway - To have our application visible to outside world, we need API Gateway. Cloud provides this facility for us.

Authentication API - We have use IAM (Identity and Access Management) to get Authenticated.

Public Endpoints - Public subnets in VPC

Internal APIs - Private subnets in VPC

OAuthorization Server 

Long Running Workers/Jobs (~30mins) 

Queues (Like RabbitMQ, Bull, etc) 

Database - We can have RDS or Aurora based on the requirement

Redis - We can make use of ElasticCache
